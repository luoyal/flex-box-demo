var express = require('express')

var app = express()

// 将express 路由按摩块加载进来
var indexRouter = require('./routes/index')
var usersRouter = require('./routes/users')


app.use('/', indexRouter)
app.use('/users', usersRouter)
app.listen(3000)
