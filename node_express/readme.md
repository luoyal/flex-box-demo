- 初始化项目 npm init

- 安装express npm install express --save

- 安装热部署库 npm install -g nodemon

- 启动node项目 nodemon server(这里看服务js叫什么)

- 模拟post请求 npm install body-parser

- 文件上传的库 npm install --save multer

- ejs 模板语言 npm install ejs

- server.js 简化版 供学习版本

- middle.js 中间件 学习版

- app.js 使用express 的router中间件 将路由按模块分割
