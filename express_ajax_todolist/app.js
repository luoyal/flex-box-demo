var express  = require('express')
var todoController = require('./controller/todoController')

var app = express()


// ejs 模板需求
app.set('view engine', 'ejs')
app.set('views', './views')

// 静态资源
app.use(express.static(__dirname + '/public'));

// 路由
todoController(app)

app.listen(3000)

console.log('You server is port 3000')

